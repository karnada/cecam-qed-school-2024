{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "df9f150a",
   "metadata": {},
   "source": [
    "# Notebook 13 - QEDFT electron-photon exchange approximation for one electron coupled to (many) photon modes in 1D \n",
    "-----------------------------------------------------------------------------------------------------------\n",
    "\n",
    "Here we look into a 1D light-matter system that consists of one electron and one photon mode using the QEDFT electron-photon exchange functional. The Kohn-Sham (KS) Hamiltonian is (in the Hartree units)\n",
    "$$\n",
    "\\hat{H}_{\\rm{KS}} = -\\frac{1}{2}\\partial_{x}^{2} + v_{\\rm{ext}}(x) + v_{\\rm{px}}(x),\n",
    "$$\n",
    "where $v_{\\rm{ext}}(x)$ is the external potential and $v_{\\rm{px}}(x)$ is the electron-photon exchange potential. \n",
    "\n",
    "From the lecture, the electron-photon exchange potential for general cases can be obtained by solving the following Poisson equation, \n",
    "$$\n",
    "\\nabla^{2}v_{{\\rm{px}}}(\\mathbf{r}) = -\\nabla\\cdot\\left[\\sum_{\\alpha=1}^{M_{p}}\\frac{\\tilde{\\lambda}_{\\alpha}^{2}}{2\\tilde{\\omega}_{\\alpha}^{2}}\\frac{(\\tilde{\\mathbf{\\varepsilon}}_{\\alpha}\\cdot\\nabla) \\left(\\mathbf{f}_{\\alpha,\\rm{px}}(\\mathbf{r})+c.c.\\right)}{\\rho(\\mathbf{r})}\\right],\n",
    "$$\n",
    "where $\\mathbf{f}_{\\alpha,\\rm{px}}(\\mathbf{r}) =\\langle(\\tilde{\\boldsymbol{\\varepsilon}}_{\\alpha}\\cdot\\mathbf{J}_{\\rm{p}})\\hat{\\mathbf{j}}_{\\rm{p}}(\\mathbf{r})\\rangle_{\\Phi}$, $\\Phi$ is the Slater-determinant constructed from the KS orbitals, $\\rho(\\mathbf{r})$ is the electron density, and $c.c.$ is the complex conjugate.\n",
    "\n",
    "The electron-photon exchange potential within the local density functional approximation (LDA) for general cases can be obtained by solving the following Poisson equation, \n",
    "$$\n",
    "\\nabla^{2} v_{{\\rm{pxLDA}}}(\\mathbf{r}) = -\\sum_{\\alpha=1}^{M_{p}}\\frac{2 \\pi^{2}\\tilde{\\lambda}_{\\alpha}^{2}}{\\tilde{\\omega}_{\\alpha}^{2}}\\left[(\\tilde{\\boldsymbol{\\varepsilon}}_{\\alpha}\\cdot\\nabla)^{2}\\left(\\frac{\\rho(\\mathbf{r})}{2V_{d}}\\right)^{\\frac{2}{d}}\\right].\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e1a960b",
   "metadata": {},
   "source": [
    "## Q. 13.1\n",
    "-------------\n",
    "\n",
    "Please derive the electron-photon exchange potential for one-electron cases in 1D system, $v_{\\rm{px}}(x)$. \n",
    "\n",
    "(Optional) A more challenge question: please derive an analytical expression for the electron-photon exchange potential in general cases, i.e., for any dimension, $v_{\\rm{px}}(\\mathbf{r})$. \n",
    "\n",
    "Please also obtain the electron-photon exchange potential with the LDA approximation for one-electron cases in 1D system, $v_{\\rm{pxLDA}}(x)$. If now we look into an isotropic cases in $d$-dimension, what is the corresponding isotropic electron-photon exchange potential? "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2dfbed5a",
   "metadata": {},
   "source": [
    "## A. 13.1\n",
    "------\n",
    "\n",
    "For one dimensional cases, the electron-photon exchange potential for one-electron cases is \n",
    "$$\n",
    "v_{\\rm{px}}(x) = \n",
    "\\left(\\sum_{\\alpha=1}^{M_{p}}\\frac{{\\tilde{\\lambda}}_{\\alpha}^{2}}{2\\tilde{\\omega}_{\\alpha}^{2}}\\right)\\frac{\\partial_{x}^{2}\\rho^{\\frac{1}{2}}(x)}{\\rho^{\\frac{1}{2}}(x)}.\n",
    "$$\n",
    "\n",
    "(Optional) The electron-photon exchange potential for one-electron cases in $d$-dimension is\n",
    "$$\n",
    "v_{\\rm{px}}(\\mathbf{r}) = \\sum_{\\alpha=1}^{M_{p}}\\frac{{\\tilde{\\lambda}}_{\\alpha}^{2}}{2\\tilde{\\omega}_{\\alpha}^{2}} \\frac{(\\tilde{\\boldsymbol{\\varepsilon}}_{\\alpha}\\cdot\\nabla)^{2}\\rho^{\\frac{1}{2}}(\\mathbf{r})}{\\rho^{\\frac{1}{2}}(\\mathbf{r})}.\n",
    "$$\n",
    "\n",
    "\n",
    "The electron-photon exchange potential within the LDA in one dimension can be obtained \n",
    "$$\n",
    "v_{\\rm{pxLDA}}(x)=-\\frac{\\pi^{2}}{8} \\left(\\sum_{\\alpha=1}^{M_{p}} \\frac{\\tilde{\\lambda}_{\\alpha}^{2}}{\\tilde{\\omega}_{\\alpha}^{2}}\\right) \\rho^{2}(x).\n",
    "$$\n",
    "\n",
    "The isotropic electron-photon exchange potential in $d$ dimension is \n",
    "$$\n",
    "v_{\\rm{pxLDA}}^{\\rm{iso}}(\\mathbf{r}) = -\\frac{2\\pi^{2}}{d} \\sum_{\\alpha=1}^{M_{p}}\\frac{\\tilde{\\lambda}_{\\alpha}^{2}}{\\tilde{\\omega}_{\\alpha}^{2}} \\left(\\frac{\\rho(\\mathbf{r})}{2V_{d}}\\right)^{\\frac{2}{d}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a0027c7",
   "metadata": {},
   "source": [
    "## Q. 13.2\n",
    "------\n",
    "\n",
    "With the analytical expression for the electron-photon exchange potential of one-electron cases in one dimension, we proceed to look into a simple one-dimensional soft hydrogen atom. We would like to know how the electron density of the one-electron system changes inside a cavity.  \n",
    "\n",
    "The potential for the one-dimensional soft hydrogen atom is \n",
    "$$\n",
    "v_{\\rm{ext}}(x) = v_{\\rm{soft}}(x) = \\frac{-1}{\\sqrt{x^{2}+a^{2}}},\n",
    "$$\n",
    "where $a$ is the softening parameter. \n",
    "\n",
    "Here we consider one photon mode with a frequency $\\omega_{\\alpha}$ and a light-matter coupling $\\lambda_{\\alpha}$. Please use $\\omega_{\\alpha} = 1$ and $\\lambda_{\\alpha} = 10^{-2}$ and $\\lambda_{\\alpha} =1$ to see how the electron density changes, compared to the outside-cavity case. \n",
    "\n",
    "The following is the general guideline: \n",
    "\n",
    "1) Write a short program to obtain the electron density of the soft hydrogen atom outside the cavity and plot its electron density;\n",
    "\n",
    "2) Construct a self-consistent-field program to solve the KS Hamiltonian with the electron-photon exchange potential within the LDA; \n",
    "\n",
    "3) Compare the electron density of the soft hydrogen atom outside and inside the cavity and see how it changes with the light-matter coupling strength and give an explanation of why the density changes in that way.\n",
    "\n",
    "Note that the results only depends on the ratio of the light-matter coupling and photon frequency, i.e., $\\lambda_{\\alpha}/\\omega_{\\alpha}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecc6eb24",
   "metadata": {},
   "source": [
    "### Still in progress "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5cdc4313",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
